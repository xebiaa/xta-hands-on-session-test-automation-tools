package com.xebia.monopoly.fitnesse.fixture;

import com.xebia.monopoly.helpers.GameHelper;

import static org.junit.Assert.assertEquals;

public class GoingToJail {

    private GameHelper gameHelper = new GameHelper();
    private int die1, die2;

    public GoingToJail() {
        gameHelper.setupGame();
    }

    public void whenPlayerThrowDoublesThreeTimesInSuccession() {
        for (int i = 1; i <= 3; i++) {
            gameHelper.setDice(1, 1);
            gameHelper.doPlayAction();
            if (i != 3) {
                assertEquals("Another roll allowed", true, gameHelper.getPlayer().isRollAllowed());
            }
        }
    }

    public boolean thenPlayerGoesToJail() {
        return !gameHelper.getPlayer().isRollAllowed();
    }
}
