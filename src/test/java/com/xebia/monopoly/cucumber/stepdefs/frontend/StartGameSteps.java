package com.xebia.monopoly.cucumber.stepdefs.frontend;

import com.github.javafaker.Faker;
import com.xebia.monopoly.pageobjects.AddUserPage;
import com.xebia.monopoly.pageobjects.PlayGamePage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import static org.hamcrest.core.Is.is;

public class StartGameSteps {

    AddUserPage addUserPage = new AddUserPage();
    PlayGamePage gamePage = new PlayGamePage();
    Faker faker = new Faker();

    @Before("@Frontend")
    public void startGame() {
        gamePage.get();
    }

    @When("^I start a game with (\\d+) players$")
    public void I_start_a_game_with_players(int playerAmount) throws Throwable {
        for (int i = 0; i < playerAmount; i++) {
                addUserPage.addUser(faker.name().fullName());
        }
        addUserPage.clickStart();
    }

    @Then("^the game can or cannot (true|false) be started$")
    public void the_game_can_or_cannot_start(boolean gameCanStart) throws Throwable {
        if (!gameCanStart) {
            addUserPage.validateModal("Need more players");
        }
        Assert.assertThat("The game started state", gamePage.isGameStarted(), is(gameCanStart));


    }

}
