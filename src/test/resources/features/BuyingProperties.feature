@BuyingProperties
Feature: Buying properties

  As a player
  I want to buy properties
  So that I can ask rent when my opponents land on it

  Scenario: Buying property
    Given I land on unowned property
    And my funds are sufficient
    When I choose to buy the property
    Then I pay the printed price
    And I receive the title deed


  Scenario: No sufficient funds
    Given I land on unowned property
    And my funds are not sufficient
    Then I am not able to buy the property

