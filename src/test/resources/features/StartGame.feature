@Frontend
@StartGame
Feature: Starting the game

  As a player
  I want to be able to play the game
  So that I can have fun with my friends

  Scenario Outline: Starting a game with various amounts of players
    When I start a game with <players> players
    Then the game can or cannot <can or cannot> be started

  Examples:
    | players | can or cannot |
    | 0       | false         |
    | 1       | false         |
    | 2       | true          |
    | 6       | true          |
